import React from 'react';
import ChampionList from './ChampionList'

const DashBoardPage = () => (
  <div>
    <ChampionList/>
  </div>
);

export default DashBoardPage;

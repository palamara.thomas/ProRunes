import React from 'react';
import { createStore } from 'redux'
import { connect } from 'react-redux';
import { setTextFilter } from '../actions/filters'
import champions from '../../champ.json';
import ChampionListItem from './ChampionListItem'


const ChampionList = (props) => {
	console.log(champions.filter(
					(champion) =>{

						return champion.name.toLowerCase().includes(props.filters.text.toLowerCase())
					}
				));
	// <img src={object.iconUrl} alt=""/>
	return (
		<div>
			<input type="text" value={props.filters.text} placeholder="Search" onChange={(e) => {
				props.dispatch(setTextFilter(e.target.value))
			}} />
			<ul>
				{champions.filter(
					(champion) => (champion.name.toLowerCase().includes(props.filters.text.toLowerCase()))
				)
					.sort(
						(a, b) => (a.name < b.name ? -1 : 1)
					)
					.map(
						(champion) => <ChampionListItem key={champion.id} {...champion} />
					)}
			</ul>
		</div>
	)
}

const mapStateToProps = (state) => {
	return {
		filters: state.filters
	}
}

export default connect(mapStateToProps)(ChampionList);
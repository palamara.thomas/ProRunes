import React from 'react';

const ChampionListItem = (props) => {
    // <img src={props.iconUrl} alt=""/>
    return (
        <li style={{ 'display': 'inline-block', 'width': '150px', 'text-align':'center' }}>
        
            <p>{props.name}</p>
        </li>
    )
}

export default ChampionListItem;
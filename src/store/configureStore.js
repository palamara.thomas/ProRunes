import { createStore, combineReducers } from "redux";
import filtersReducer from '../reducers/filters';

//Store creation we put the store creation inside of a function so it can be exported 

export default () => {
    const store = createStore(
        combineReducers({
            filters: filtersReducer
        })
    );
return store;
}